'use strict';

var fs = require('fs');
const prefix = "bingo_game_";
const cardPrefix = "bingo_card_";
const folder = "./games/";

exports.list_all_games = function(req, res) {
	var games = [];

	fs.readdirSync(folder).forEach(file => {
		console.log(file);
		if (file.startsWith(prefix)) {
			games.push(file.substr(prefix.length));

		}
	})
	
	res.json(games);

};

exports.create_a_game = function(req, res) {
	var bingo_game = req.body;
	console.log('create a game called ' + bingo_game.name);

	fs.openSync(folder + prefix + bingo_game.name, "w");

	res.json(bingo_game);
};

exports.get_a_game = function(req, res) {
	console.log('get a game: ' + req.params.game);
	try {
		var myGame = fs.readFileSync(folder + prefix + req.params.game, 'utf8');
		res.json(myGame);
	} catch (err) {
		res.json("Game not found.")
	}
};

exports.update_a_game = function(req, res) {
	console.log('update a game');
};

exports.delete_a_game = function(req, res) {
	console.log('delete a game');
	fs.unlinkSync(folder + prefix + req.params.game);
	res.json({ message: 'game deleted' } );
};

exports.list_all_cards = function(req, res) {
	try {
		var myGame = req.params.game;
		var myCardPrefix = cardPrefix + myGame;

		var cards = [];
		console.log('list all cards: ' + req.params.game);
		var myGame = fs.readFileSync(folder + prefix + req.params.game, 'utf8');

		fs.readdirSync(folder).forEach(file => {
			console.log(file);
			if (file.startsWith(myCardPrefix)) {
				var myCard = file.substr(myCardPrefix.length);
				if (myCard.length > 0) {
					cards.push(myCard);
				}

			}
		})
		
		res.json(cards);
	} catch (err) {
		res.json("Error");
	}
};

exports.create_a_card = function(req, res) {
	try {
		var myGame = req.params.game;
		var myCard = req.body.name;
		var myNumbers = req.body.numbers;

		var f = fs.openSync(folder + cardPrefix + myGame + myCard, "w");
		fs.writeSync(f, myNumbers);
		fs.closeSync(f);

		res.json(myNumbers);
	} catch (err) {
		res.json("Error");
	}
};

exports.get_a_card = function(req, res) {
	try {
		var myGame = req.params.game;
		var myCard = req.params.card;


		var myNumbers = fs.readFileSync(folder + cardPrefix + myGame + myCard, 'utf8');

		res.json(myNumbers);
	} catch (err) {
		res.json("Error");
	}
};

