'use strict';

module.exports = function(app) {
	var bingo = require('../controllers/bingoController');

	app.route('/games')
		.get(bingo.list_all_games)
		.post(bingo.create_a_game);

	app.route('/games/:game')
		.get(bingo.get_a_game)
		.put(bingo.update_a_game)
		.delete(bingo.delete_a_game);
	
	app.route('/games/:game/cards')
		.get(bingo.list_all_cards)
		.post(bingo.create_a_card);

	app.route('/games/:game/cards/:card')
		.get(bingo.get_a_card);


};
