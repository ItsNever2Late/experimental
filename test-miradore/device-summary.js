
var miradoreAPIKey = "2_aeUmz%7BAY%5DO0a7tt";

var parser = require('xml2json');

var Client = require('node-rest-client').Client;
var options = {
    object: false,
    reversible: false,
    coerce: false,
    sanitize: true,
    trim: true,
    arrayNotation: false,
    alternateTextNode: false
}; 
var client = new Client();

if (process.argv.length <= 2) {
	console.log("Usage: dvice-summary.js [tag]");
	process.exit();
} else {
	tag = process.argv[2];
}
client.parsers.find("XML").options = options;
  
// direct way 
var URL = "https://itsnever2late.online.miradore.com/API/Device?auth=" + miradoreAPIKey + "&select=Tag.Name,InvDevice.SerialNumber,InvApplication.Name,InvApplication.Version&filters=Tag.Name%20contains%20" + tag + "&options=rows=1000";
// console.log(URL);
client.get(URL, function (data, response) {
    // parsed response body as js object 
//    console.log(data.Content.Items[0].Device[0].InvDevice);
	var devices = data.Content.Items[0].Device;
	var tags, serial_num, app_name, app_version;

	for (var i = 0; i < devices.length; i++) {
		var device = devices[i];
		serial_num = device.InvDevice[0].SerialNumber[0];
		tags = "";
		for (var j = 0; j < device.Tags.length; j++) {
			for (var k = 0; k < device.Tags[j].Tag.length; k++) {
				if (tags == "") {
					tags = device.Tags[j].Tag[k].Name[0];
				} else {
					tags = tags + "," + device.Tags[j].Tag[k].Name[0];
				}
			}
		}
		app_name = "";
		app_version = "";

		for (var j = 0; j < device.InvApplications[0].InvApplication.length; j++) {
			var app = device.InvApplications[0].InvApplication[j];
			if (app.Name == "Olympus") {
				app_name = app.Name[0];
				if (app.Version) {
					app_version = app.Version[0];
				}
			}
		}
//		console.log(device);
		console.log(serial_num + "|" + tags +"|" + app_name + "|" + app_version);
	}
    // raw response 
    // console.log(response);
});

