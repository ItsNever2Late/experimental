$(function(){ 
  var store = new Store('example', { remote: 'http://localhost:4984/example', PouchDB: PouchDB });
  
//  var content_store = new Store('content_meta_data', {remote: 'https://olympusqa:$z*J7QZW@386Bq8Bkcr7@syncgw-qa.in2lolympus.com:4984/content_meta_data', PouchDB: PouchDB });
  var content_store = new Store('content_meta_data', {remote: 'http://olympusprod:TA8*z7HG$QnyKtc5n&5v@syncgw.in2l:4984/content_meta_data', PouchDB: PouchDB });
  content_store.connect();

	$('#contactForm').submit(function(event) {
    event.preventDefault();

    var name = $('#name').val();
    var email = $('#email').val();
    var mobile = $('#mobile').val();

    // Save the contact to the database
    store.add({
      name: name,
      mobile: mobile,
      email: email
    });

    $('#contactForm')[0].reset();
  });

  //add new contact to the page
  function addNewContactToList(contact) {
    var newContact = '<tr><td>' + contact.name + '</td><td>' + contact.mobile + '</td><td>' + contact.email + '</td></tr>'
    $("#contactList tbody").append(newContact);
  }

  //when a new entry is added to the database, run the corresponding function
  store.on('add', addNewContactToList);

  function loadContacts() {
    content_store.findAll().then(function(contacts) {
      var tbody = '<tr>';
      var ids;
      $.each(contacts, function (i, contact) {
        if (contact.type == 'root_container') {
          ids = contact.children;
        }
      });

      var row_pos = 1;
      var col_pos = 1;
      console.log(ids);

      $.each(ids, function (i, my_doc) { 

          var img_html = '';
          var row = '<td id="' + my_doc + '">'  + '</td>';
          console.log(tbody);

          tbody += row;
          if (col_pos == 3) {
            row_pos += 1;
            col_pos = 1;
            tbody += "</tr><tr>";
          } else {
            col_pos += 1;
          }
      });

      tbody += "</tr>";


      $("#contactList tbody").html('').html(tbody);

      $.each(contacts, function (i, contact) {
        if (ids.indexOf(contact.id) >=0) {
          var my_doc = contact.id;
          var img_html = '';

          content_store.db.get(my_doc, {attachments: true}).then(function (doc) {
            
            return content_store.db.getAttachment(my_doc, 'tile_image');
          }).then(function (blob) {
            var url = URL.createObjectURL(blob);
            img = document.createElement('img');
            img.src = url;
            img.height = 200;
            img.width = 300;


            // console.log('should append this to row: ' + row);
            document.getElementById(my_doc).appendChild(img);
            document.getElementById(my_doc).appendChild(document.createTextNode(contact.title));
//            document.body.appendChild(img);
          }).catch(function (err) {
            console.log('doc: ' + my_doc);
            console.log(err);
          });

        }
      });

    });
  }

  // when the site loads in the browser,
  // we load all previously saved contacts from hoodie
  loadContacts();

  // store.connect();
});