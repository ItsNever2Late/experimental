A copy of a phonebook web app modified to demonstrate syncing from in2l Couchbase metadata into PouchDB in the browser.

Notes
* Must be run under a web server (I used a small http server chrome extension)

* URL must be http://localhost:4200/ in order to ensure the Sync Gateway allows the browser to sync from it

* Pouchdb looks for the Vagrant named sync gateway host to sync to

* No error handling

* Should render a page with content and thumbnails

* Once synced, should render even when sync gw is unreachable

* Currently does NOT store the app in browser, so it won't work when the web server is offline (although that's possible looking at this example) https://wiki-offline.jakearchibald.com/

